## Отчеты

**1. GET /api​/v1​/Activities**

* URL https://fakerestapi.azurewebsites.net/api/v1/Activities

* Ожидаемый результат: Код ответа: 200,тест успешно пройден

* Заголовки запроса: 

User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: ca62ffd8-484f-4d76-b331-13e98263b77a
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

* Тело запроса: отсутствует 

* Заголовки ответа:

Content-Type: application/json; charset=utf-8; v=1.0
Date: Sat, 17 Jun 2023 14:40:09 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0

* Тело ответа:
```
[
  {
    "id": 1,
    "title": "Activity 1",
    "dueDate": "2023-06-17T15:30:50.9363662+00:00",
    "completed": false
  }
```

**2. POST /api​/v1​/Activities**

* URL https://fakerestapi.azurewebsites.net/api/v1/Activities

* Ожидаемый результат: Код ответа: 201 Created, тест успешно пройден

* Заголовки запроса:

Content-Type: application/json
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 298c6e4e-3f87-4a05-b787-2e3c3184530a
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

* Тело запроса:
```
{
  "id": 0,
  "title": "string",
  "dueDate": "2023-06-17T14:50:01.870Z",
  "completed": true
}
```
* Заголовки ответа:

Content-Type: application/json; charset=utf-8; v=1.0
Date: Sat, 17 Jun 2023 14:51:30 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0

* Тело ответа:
```
{
  "id": 0,
  "title": "string",
  "dueDate": "2023-06-17T14:50:01.87Z",
  "completed": true
}
```

**3. POST /api​/v1​/Activities:: ввод в тело  запроса "id"null**

* URL https:https://fakerestapi.azurewebsites.net/api/v1/Activities

* Ожидаемый результат: Код ответа: 400, тест успешно пройден

* Заголовки ответа:

Content-Type: application/problem+json; charset=utf-8
Date: Sat, 17 Jun 2023 15:00:43 GMT
Server: Kestrel
Transfer-Encoding: chunked 
 
* Тело запроса:
```
{
  "id": null,
  "title": "string",
  "dueDate": "2023-06-17T14:59:20.749Z",
  "completed": true
}
``` 
 
* Заголовки запроса:

Content-Type: application/json
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: cbd3e392-48a3-4a2b-9a37-af03e8efb06f
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

* Тело ответа

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-88158623fcaa2448b3322555e0f8893a-236c9508e01a4c49-00","errors":{"$.id":["The JSON value could not be converted to System.Int32. Path: $.id | LineNumber: 1 | BytePositionInLine: 12."]}}
```
**4. POST /api​/v1​/Activities: :удаление скобки } в теле запроса**

* URL https:https://fakerestapi.azurewebsites.net/api/v1/Activities

* Ожидаемый результат: Код ответа: 400, тест успешно пройден

* Заголовки ответа:

Content-Type: application/problem+json; charset=utf-8
Date: Sat, 17 Jun 2023 15:09:30 GMT
Server: Kestrel
Transfer-Encoding: chunked

* Тело запроса:
```
{
  "id": null,
  "title": "string",
  "dueDate": "2023-06-17T14:59:20.749Z",
  "completed": true
``` 
 
* Заголовки запроса:

Content-Type: application/json
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 4ea52376-1353-4d68-95eb-1fce36c47928
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

* Тело ответа:

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-c49df228718c7e498070319b3268b29c-67816bdf33ed1b4b-00","errors":{"$":["Expected depth to be zero at the end of the JSON payload. There is an open JSON object or array that should be closed. Path: $ | LineNumber: 4 | BytePositionInLine: 19."]}}
```

**5. POST /api/v1/Activities: удаление строки  "title": "string"**

* URL https:https://fakerestapi.azurewebsites.net/api/v1/Activities

* Ожидаемый результат: Код ответа: 400 Bad Request,тест успешно пройден

* Заголовки ответа:

Content-Type: application/problem+json; charset=utf-8
Date: Sat, 17 Jun 2023 16:13:45 GMT
Server: Kestrel
Transfer-Encoding: chunked

* Тело ответа:

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-876b17f5dc523f4db8df26c3bcf6c096-95a4e6a0d7188c4d-00","errors":{"$":["Expected depth to be zero at the end of the JSON payload. There is an open JSON object or array that should be closed. Path: $ | LineNumber: 4 | BytePositionInLine: 19."]}}
```
* Заголовки запроса:

Content-Type: application/json
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 48878400-98f3-4cd0-8e34-964e67ca883b
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

* Тело запроса:

```
{
  "id": 0,
 
  "dueDate": "2023-06-17T15:08:12.745Z",
  "completed": true
```

**6. GET ​/api​/v1​/Activities​/{2}**

* URL https:https://fakerestapi.azurewebsites.net/api/v1/Activities/{{activity_id}}

* Ожидаемый результат: Код ответа: 200,тест успешно пройден

* Заголовки ответа:

Content-Type: application/json; charset=utf-8; v=1.0
Date: Sat, 17 Jun 2023 16:22:45 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0

* Тело запроса:отсутствует

* Заголовки запроса:

User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: ef1adc2c-709f-4638-aa9c-90f68d5c2e02
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

* Тело ответа:

```
{"id":2,"title":"Activity 2","dueDate":"2023-06-17T18:22:46.3746341+00:00","completed":true}
```

**7. GET ​/api​/v1​/Activities​/{0}**

* URL https:https://fakerestapi.azurewebsites.net/api/v1/Activities/{{activ_id}}

* Ожидаемый результат: Код ответа: 404,тест успешно пройден

* Заголовки ответа:

Content-Type: application/problem+json; charset=utf-8; v=1.0
Date: Sat, 17 Jun 2023 16:28:29 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0

* Тело запроса:"∅"

* Заголовки запроса:

User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 1044233d-fd0e-4eeb-ab83-9cb599f030d7
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

* Тело ответа:

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.4","title":"Not Found","status":404,"traceId":"00-de95acddf9318c448d37538855dfe0d1-640548ba6e70d444-00"}
```  

**8.  PUT ​/api​/v1​/Activities​/{3}**

* URL https:https://fakerestapi.azurewebsites.net/api/v1/Activities/{{activ_for_put}}

* Ожидаемый результат: Код ответа: 200 OK,тест успешно пройден

* Заголовки ответа:

Content-Type: application/json; charset=utf-8; v=1.0
Date: Sat, 17 Jun 2023 16:38:42 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0

* Тело запроса:

```
{
  "id": 0,
  "title": "string",
  "dueDate": "2023-06-17T16:37:26.696Z",
  "completed": true
}
```

* Заголовки запроса:

Content-Type: application/json
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 23076e96-6587-438c-935d-d82a02552dde
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

* Тело ответа:
```
{"id":0,"title":"string","dueDate":"2023-06-17T16:37:26.696Z","completed":true}
```

**9. PUT ​/api​/v1​/Activities​/{888888888888}**

* URL https:https://fakerestapi.azurewebsites.net/api/v1/Activities/888888888888{{ac_put}}

* Ожидаемый результат: Код ответа: 400,тест успешно пройден

* Заголовки запроса:

Content-Type: application/json
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 76cf62c9-1198-4e8c-9f32-05fc91be98d9
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

* Тело запроса:
```
{
  "id": 0,
  "title": "string",
  "dueDate": "2023-06-17T16:51:57.013Z",
  "completed": true
}
```	
* Заголовки ответа:

Content-Type: application/problem+json; charset=utf-8
Date: Sat, 17 Jun 2023 16:53:07 GMT
Server: Kestrel
Transfer-Encoding: chunked

* Тело ответа:			
```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-b5f59f7cf0f92a4fac6c49b9a04ecdde-f70d94b4f86e7d4d-00","errors":{"id":["The value '888888888888888888888888' is not valid."]}}
```

**10. PUT ​/api​/v1​/Activities​/{-3} введение отрицательного числа**

* URL https:https://fakerestapi.azurewebsites.net/api/v1/Activities/{{activity_minus}}

* Ожидаемый результат: Код ответа: 400, фактически 200,тест не проейден

* Заголовки запроса:

Content-Type: application/json
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 82e82ab8-ccdf-4336-8c9b-2c4a295de8af
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

* Тело запроса:
```
{
  "id": 0,
  "title": "string",
  "dueDate": "2023-06-17T17:09:30.083Z",
  "completed": true
}
```
* Заголовки ответа:

Content-Type: application/json; charset=utf-8; v=1.0
Date: Sat, 17 Jun 2023 17:11:14 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0

* Тело ответа:
```
{
  "id": 0,
  "title": "string",
  "dueDate": "2023-06-17T17:09:30.083Z",
  "completed": true
}
```

**11. PUT ​/api​/v1​/Activities​/{8} ввод в тело запроса "id" : null**

* URL https:https://fakerestapi.azurewebsites.net/api/v1/Activities/{{activity_put}}

* Ожидаемый результат: Код ответа: 400,тест успешно пройден

* Заголовки запроса:

Content-Type: application/json
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 82d29e05-176a-4f88-9ee8-d2fda18cd6f5
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

* Тело запроса:
```
{
  "id": null,
  "title": "string",
  "dueDate": "2023-06-17T17:50:26.483Z",
  "completed": true
}
```

* Заголовки ответа:

Content-Type: application/problem+json; charset=utf-8
Date: Sat, 17 Jun 2023 17:52:05 GMT
Server: Kestrel
Transfer-Encoding: chunked

* Тело ответа: 
```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-d07dd4eaf890044d86dc4e807a5b87a2-b9fb23713c054448-00","errors":{"$.id":["The JSON value could not be converted to System.Int32. Path: $.id | LineNumber: 1 | BytePositionInLine: 12."]}}
```

**12. PUT /api/v1/Activities{8} :удаление скобки "}" в теле запроса**

* URL https: https://fakerestapi.azurewebsites.net/api/v1/Activities/{{activity_put}}

* Ожидаемый результат: Код ответа: 400,тест успешно пройден

* Заголовки запроса:

Content-Type: application/json
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 3a07fe90-423f-42df-9797-c1abbba55c34
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

* Тело запроса:
```
{
  "id": 0,
  "title": "string",
  "dueDate": "2023-06-17T17:32:47.116Z",
  "completed": true
```
* Заголовки ответа:

Content-Type: application/problem+json; charset=utf-8
Date: Sat, 17 Jun 2023 17:34:13 GMT
Server: Kestrel
Transfer-Encoding: chunked

* Тело ответа:
```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-6f661f563ae4024f8ce0a12fc8356fa5-c61a9e72f4228643-00","errors":{"$":["Expected depth to be zero at the end of the JSON payload. There is an open JSON object or array that should be closed. Path: $ | LineNumber: 4 | BytePositionInLine: 19."]}}
```

**13. PUT /api/v1/Activities{5} :удаление тела запроса**

* URL https:https://fakerestapi.azurewebsites.net/api/v1/Activities/{{activities_id_put}}

* Ожидаемый результат: Код ответа: 415,тест успешно пройден

* Заголовки запроса:

User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: ea7616cf-64bf-4b54-9177-55c522ca78c6
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

* Тело запроса:удалено

* Заголовки ответа:

Content-Type: application/problem+json; charset=utf-8
Date: Sat, 17 Jun 2023 18:28:28 GMT
Server: Kestrel
Transfer-Encoding: chunked

* Тело ответа:
```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.13","title":"Unsupported Media Type","status":415,"traceId":"00-7d950b0dbe6dac478ca9c16bcb5f4cb3-31f57aa7371aab4e-00"}
```

**14. DELETE ​/api​/v1​/Activities​/{10}**

* URL https:https://fakerestapi.azurewebsites.net/api/v1/Activities/{{activ_del}}

* Ожидаемый результат: Код ответа: 200 OK, тест успешно пройден

* Заголовки запроса:

User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 3880f3dd-5402-4d3d-b3e4-29055dfa65c2
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

* Тело запроса:отсутствует

* Заголовки ответа:

Content-Length: 0
Date: Sat, 17 Jun 2023 18:36:48 GMT
Server: Kestrel
api-supported-versions: 1.0

* Тело ответа:отсутствует

**15. DELETE ​/api​/v1​/Activities​/{45454545454545}**

* URL https:https://fakerestapi.azurewebsites.net/api/v1/Activities/{{actitity_del_id}}

* Ожидаемый результат: Код ответа: 400,тест успешно пройден

* Заголовки запроса:

User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 28cf287e-c025-4d53-a107-c629be0b7881
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

* Тело запроса:отсутствует

* Заголовки ответа:

Content-Type: application/problem+json; charset=utf-8
Date: Sat, 17 Jun 2023 18:41:31 GMT
Server: Kestrel
Transfer-Encoding: chunked

* Тело ответа:
```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-34259cc1990c964698ec25f7f51b1aa2-27096b8fcb5ca643-00","errors":{"id":["The value '45454545454545' is not valid."]}}
```

**16. GET /api/v1/CoverPhotos** 

* URL https:https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos

* Ожидаемый результат: Код ответа: 200,тест успешно пройден

* Заголовки запроса:

User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 1aba71ca-828f-430b-8fc4-565644d712f3
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

* Тело запроса:отсутствует

* Заголовки ответа:

Content-Type: application/json; charset=utf-8; v=1.0
Date: Sat, 17 Jun 2023 18:49:27 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0

* Тело ответа:
```
[
  {
    "id": 1,
    "idBook": 1,
    "url": "https://placeholdit.imgix.net/~text?txtsize=33&txt=Book 1&w=250&h=350"
  },
```

**17. POST /api/v1/CoverPhotos**  

* URL https:https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos

* Ожидаемый результат: Код ответа: 201 Created,тест успешно пройден

* Заголовки запроса:

Content-Type: application/json
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: c004b099-471d-4c60-9d1a-bb53c2040e04
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

* Тело запроса:
```
{
  "id": 0,
  "idBook": 0,
  "url": "string"
}
```
* Заголовки ответа:

Content-Type: application/json; charset=utf-8; v=1.0
Date: Sat, 17 Jun 2023 18:55:47 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0

* Тело ответа:
```
{"id":0,"idBook":0,"url":"string"}
```

**18. POST /api/v1/CoverPhotos: замена знака "," на ";" в теле запроса**

* URL https:https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos

* Ожидаемый результат: Код ответа: 400,тест успешно пройден

* Заголовки запроса:

Content-Type: application/json
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 46df6fbf-eb41-4104-99e3-69e841af5b04
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

* Тело запроса:
```
{
  "id": 0;
  "idBook": 0;
  "url": "string"
}
```

* Заголовки ответа:

Content-Type: application/problem+json; charset=utf-8
Date: Sat, 17 Jun 2023 19:03:01 GMT
Server: Kestrel
Transfer-Encoding: chunked

* Тело ответа:
```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-8044883f7ee25c4f91ba226aed642038-ebe2156538407249-00","errors":{"$.id":["';' is an invalid end of a number. Expected a delimiter. Path: $.id | LineNumber: 1 | BytePositionInLine: 9."]}}
```

**19. POST /api/v1/CoverPhotos: замена "{ }" на  "[]" в теле запроса**

* URL https: https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos

* Ожидаемый результат: Код ответа: 400,тест успешно пройден

* Заголовки запроса:

Content-Type: application/json
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: b2c53bd3-12b8-4dd4-9f31-4f9f483303d1
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

* Тело запроса:
```
[
  "id": 0,
  "idBook": 0,
  "url": "string"
]
```

* Заголовки ответа:

Content-Type: application/problem+json; charset=utf-8
Date: Sat, 17 Jun 2023 19:07:21 GMT
Server: Kestrel
Transfer-Encoding: chunked

* Тело ответа:
```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-4ec658dede77d649a910dcf36ffc631a-d59674188ecda149-00","errors":{"$":["The JSON value could not be converted to FakeRestAPI.Models.CoverPhoto. Path: $ | LineNumber: 0 | BytePositionInLine: 1."]}}
```

**20. GET ​/api​/v1​/CoverPhotos​/books​/covers​/{4Book}**

* URL: https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/books/covers/{{coverPhotos_id}}

* Ожидаемый результат: Код ответа: 200 OK,тест успешно пройден

* Заголовки запроса:

User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 86cab270-b20e-4538-8423-01352eaf7bf0
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

* Тело запроса: отсутствует

* Заголовки ответа:

Content-Type: application/json; charset=utf-8; v=1.0
Date: Sat, 17 Jun 2023 19:16:47 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0

* Тело ответа:
```
[{"id":4,"idBook":4,"url":"https://placeholdit.imgix.net/~text?txtsize=33&txt=Book 4&w=250&h=350"}]
```

**21. GET ​/api​/v1​/CoverPhotos​/books​/covers​/{78945612356Book}**

* URL: https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/books/covers/{{book_id}}

* Ожидаемый результат:Код ответа: 400,тест успешно пройден

* Заголовки запроса:

User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 3d86dbd4-7a6c-41cb-ab60-f8ac408a8bea
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

* Тело запроса:отсутствует

* Заголовки ответа:

Content-Type: application/problem+json; charset=utf-8
Date: Sat, 17 Jun 2023 19:21:45 GMT
Server: Kestrel
Transfer-Encoding: chunked

* Тело ответа:
```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-d69c3e95c6115a4a86b8307f4dad5c69-315996d427236b4b-00","errors":{"idBook":["The value '78945612356' is not valid."]}}
```

**22. GET /api/v1/CoverPhotos/{15}**

* URL: https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/{{cover_photos_id}}

* Ожидаемый результат:

* Заголовки запроса:

User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 320eea5e-a9ba-45f3-a738-8d55385efa29
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

* Тело запроса: отсутствует

* Заголовки ответа:

Content-Type: application/json; charset=utf-8; v=1.0
Date: Sat, 17 Jun 2023 19:26:38 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0

* Тело ответа:
```
{"id":15,"idBook":15,"url":"https://placeholdit.imgix.net/~text?txtsize=33&txt=Book 15&w=250&h=350"}
```

**23. GET /api/v1/CoverPhotos/{123456789}**

* URL: https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/{{photos_id}}

* Ожидаемый результат: Код ответа: 404,тест успешно пройден

* Заголовки запроса:

User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 69fac365-14b4-4162-8bff-5669bb8869de
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

* Тело запроса: отсутствует

* Заголовки ответа:

Content-Type: application/problem+json; charset=utf-8; v=1.0
Date: Sat, 17 Jun 2023 19:38:58 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0

* Тело ответа:
```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.4","title":"Not Found","status":404,"traceId":"00-2c715ebeb46ebc4d9d800b2ce4b721e3-b897a852b054dd40-00"}
```

**24. PUT /api/v1/CoverPhotos/{8}**

* URL: https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/{{activity_put}}

* Ожидаемый результат: Код ответа: 200 OK,тест успешно пройден

* Заголовки запроса:

Content-Type: application/json
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: de4203fa-04f7-4ef5-8294-08ff838a85b7
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

* Тело запроса:
```
{
  "id": 0,
  "idBook": 0,
  "url": "string"
}
```
* Заголовки ответа:

Content-Type: application/json; charset=utf-8; v=1.0
Date: Sat, 17 Jun 2023 19:43:36 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0

* Тело ответа
```
{"id":0,"idBook":0,"url":"string"}
```

**25. PUT /api/v1/CoverPhotos/{4}:добавление в тело запроса двойных запятых ",,"**

* URL: https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/{{coverPhotos_id}}

* Ожидаемый результат: Код ответа: 200 OK,тест успешно пройден

* Заголовки запроса:

Content-Type: application/json
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 46180886-5b8e-443b-a47c-5ce6577db8f1
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

* Тело запроса:
```
{
  "id": 0,,
  "idBook": 0,,
  "url": "string"
}
```

* Заголовки ответа:

Content-Type: application/json; charset=utf-8; v=1.0
Date: Sat, 17 Jun 2023 19:50:15 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0

* Тело ответа
```
{"id":4,"idBook":4,"url":"https://placeholdit.imgix.net/~text?txtsize=33&txt=Book 4&w=250&h=350"}
```

**26. PUT /api/v1/CoverPhotos/{3}: изменение "id": 0 на "null"**

* URL: https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/{{activ_for_put}}

* Ожидаемый результат: Код ответа: 400,тест успешно пройден

* Заголовки запроса:

quest Headers
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 77dc9d85-da6f-4fa9-8790-b88982e930e5
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

* Тело запроса:
```
{
  "id": null,
  "idBook": 0,
  "url": "string"
}
```

* Заголовки ответа:

Content-Type: application/problem+json; charset=utf-8
Date: Sat, 17 Jun 2023 19:54:53 GMT
Server: Kestrel
Transfer-Encoding: chunked

* Тело ответа:
```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-633d5df92a1cc140afc2eae6e30b7b81-60c20d9eb95cb24f-00","errors":{"$.id":["The JSON value could not be converted to System.Int32. Path: $.id | LineNumber: 1 | BytePositionInLine: 12."]}}
```

**27. PUT /api/v1/CoverPhotos/{-85}:ввод отрицательного числа**

* URL: https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/{{cover_id}}

* Ожидаемый результат: HTTP статус: 400 (Bad Request - Плохой запрос)

* Заголовки запроса:

Content-Type: application/json
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: c039acab-504b-4c82-9501-f4b3bcc57075
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

* Тело запроса:
```
{
  "id": 0,
  "idBook": 0,
  "url": "string"
}
```

* Заголовки ответа:

Content-Type: application/json; charset=utf-8; v=1.0
Date: Sat, 17 Jun 2023 20:00:42 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0

* Тело ответа:
```
{"id":0,"idBook":0,"url":"string"}
```

**28.  DELETE ​/api​/v1​/CoverPhotos​/{14}**

* URL: https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/{{cov_ph_id}}

* Ожидаемый результат: Код ответа: 200 OK,тест успешно пройден

* Заголовки запроса:

User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: ecf07a9d-dfec-4515-a3ca-4bbd0628def7
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

* Тело запроса:отсутствует

* Заголовки ответа:

Content-Length: 0
Date: Sat, 17 Jun 2023 20:05:31 GMT
Server: Kestrel
api-supported-versions: 1.0

* Тело ответа:отсутствует

**29. DELETE ​/api​/v1​/CoverPhotos​/{8582807471}**

* URL: https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/{{cp_id}}

* Ожидаемый результат: HTTP статус: 400 (Bad Request - Плохой запрос), тест успешно пройден

* Заголовки запроса:

User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: ed6d9853-efa1-4964-9fe9-b76cc6994e48
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

* Тело запроса: отсутствует

* Заголовки ответа:

Content-Type: application/problem+json; charset=utf-8
Date: Sat, 17 Jun 2023 20:09:49 GMT
Server: Kestrel
Transfer-Encoding: chunked

* Тело ответа:
```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-677f3e51b173234ca2f3beb56d7de9b4-15d7882ea7f4004b-00","errors":{"id":["The value '8582807471' is not valid."]}}
```

## Authors

**30. GET ​/api​/v1​/Authors**

* URL <https://fakerestapi.azurewebsites.net/api/v1/Authors>

* Ожидаемый результат: 200, тест успешно пройден

* Заголовки запроса:
```
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 407bb9f4-69df-4949-bd53-683ef9c1dc85
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

```
* Тело запроса:

отсутствует

* Ожидаемый результат:
```
Content-Type: application/json; charset=utf-8; v=1.0
Date: Sat, 17 Jun 2023 12:28:41 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0
```
* Тело ответа:
```
[{"id":1,"idBook":1,"firstName":"First Name 1","lastName":"Last Name 1"},{"id":2,"idBook":1,"firstName":"First Name 2","lastName":"Last Name 2"},
```

**31. POST ​/api​/v1​/Authors**

* URL <https://fakerestapi.azurewebsites.net/api/v1/Authors>

* Ожидаемый результат: HTTP статус: 201 Created

* Заголовки запроса:
```
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 407bb9f4-69df-4949-bd53-683ef9c1dc85
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

```
* Тело запроса:
```
{ "id": 0, "idBook": 0, 
"firstName": "строка", 
"lastName": "строка" }
```
* Ожидаемый результат:
```
Content-Type: application/json; charset=utf-8; v=1.0
Date: Sat, 17 Jun 2023 12:28:41 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0
```
* Тело ответа:
```
{"id":1,"idBook":1,"firstName":"First Name 1","lastName":"Last Name 1"},{"id":2,"idBook":1,"firstName":"First Name 2","lastName":"Last Name 2"},
```

**32. POST ​/api​/v1​/Authors - ввод в поле запроса "id" - null**

* URL <https://fakerestapi.azurewebsites.net/api/v1/Authors>

* Ожидаемый результат: 400 Error: Bad Request

* Заголовки запроса:
```
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 177f36ab-1b00-4a66-8813-4a9f1b6d6212
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

```
* Тело запроса:
```
{
  "id": {{Authors_id}},
  "idBook": 0,
  "firstName": "string",
  "lastName": "string"
}
```
* Ожидаемый результат:
```
Content-Type: application/problem+json; charset=utf-8
Date: Sat, 17 Jun 2023 13:09:49 GMT
Server: Kestrel
Transfer-Encoding: chunked
```
* Тело ответа:
```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-23b2da990034ed4b8094337bf2cdfd96-841ac5b6e6247341-00","errors":{"$.id":["The JSON value could not be converted to System.Int32. Path: $.id | LineNumber: 1 | BytePositionInLine: 9."]}}
```

**33. POST /api​/v1​/Authors - ввод в поле запроса "id" - 0987654321**

* URL <https://fakerestapi.azurewebsites.net/api/v1/Authors>

* Ожидаемый результат: 400 Error: Bad Request

* Заголовки запроса:
```
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 47abb561-ea03-4fce-8577-100f1006c1ce
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

```
* Тело запроса:
```
{
  "id": 0987654321,
  "idBook": 0,
  "firstName": "string",
  "lastName": "string"
}
```
* Ожидаемый результат:
```
Content-Type: application/problem+json; charset=utf-8
Date: Sat, 17 Jun 2023 13:31:39 GMT
Server: Kestrel
Transfer-Encoding: chunked
```
* Тело ответа:
```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-155072e845fa884f8ce1ab4670a59ddb-4ad598918eac784b-00","errors":{"$.id":["Invalid leading zero before '9'. Path: $.id | LineNumber: 1 | BytePositionInLine: 9."]}}
```

**34. POST​/api​/v1​/Authors - удаление скобки { в теле запроса**

* URL https://fakerestapi.azurewebsites.net/api/v1/Authors

* Ожидаемый результат: 400 Error: Bad Request

* Заголовки запроса:
```
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 26eadc52-bf2a-4aeb-ab5a-ea0f21efcb8a
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
```
* Тело запроса:
```
 "id": {{Authors_id}},
  "idBook": 0,
  "firstName": "string",
  "lastName": "string"
}
```
* Ожидаемый результат:
```
Content-Type: application/problem+json; charset=utf-8
Date: Sat, 17 Jun 2023 14:02:04 GMT
Server: Kestrel
Transfer-Encoding: chunked
```
* Тело ответа:
```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-c6d521529c66e248a3b47ea394ace821-8ae97f5b81018e4e-00","errors":{"$":["The JSON value could not be converted to FakeRestAPI.Models.Author. Path: $ | LineNumber: 0 | BytePositionInLine: 5."]}}
```

**35. POST​/api​/v1​/Authors - удаление в теле запроса строки "firstName": "string"**

* URL https://fakerestapi.azurewebsites.net/api/v1/Authors

* Ожидаемый результат: 400 Error: Bad Request (фактический 200)

* Заголовки запроса:
```
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 26eadc52-bf2a-4aeb-ab5a-ea0f21efcb8a
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
```
* Тело запроса:
```
{
  "id": 0,
  "idBook": 0,
 
  "lastName": "string"
}
```
* Ожидаемый результат:
```
Content-Type: application/json; charset=utf-8; v=1.0
Date: Sat, 17 Jun 2023 14:15:37 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0
```
* Тело ответа:
```
{"id":0,"idBook":0,"firstName":null,"lastName":"string"}
```

**36. GET ​/api​/v1​/Authors​/authors​/books​/{idBook}**
* URL https://fakerestapi.azurewebsites.net/api/v1/Authors/authors/books/{{id_book}}

* Ожидаемый результат: 200, тест успешно пройден

* Заголовки запроса:
```
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 7ef82cca-9ba2-4db5-8467-901572835637
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
```
* Тело запроса:

отсутствует

* Ожидаемый результат:
```
Content-Type: application/json; charset=utf-8; v=1.0
Date: Sat, 17 Jun 2023 14:15:37 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0
```
* Тело ответа:
```
[{"id":1,"idBook":1,"firstName":"First Name 1","lastName":"Last Name 1"},{"id":2,"idBook":1,"firstName":"First Name 2","lastName":"Last Name 2"},{"id":3,"idBook":1,"firstName":"First Name 3","lastName":"Last Name 3"}]
```

**37. GET ​/api​/v1​/Authors​/authors​/books​/{idBook} - ввод {0}**

* URL https://fakerestapi.azurewebsites.net/api/v1/Authors{{Authors_book_id}}

* Ожидаемый результат: 404 Not Found (фактически 200)

* Заголовки запроса:
```
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 5ab85321-70da-4df9-9c19-dde8fad43070
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
```
* Тело запроса:

отсутствует

* Ожидаемый результат:
```
Content-Length: 0
Date: Sat, 17 Jun 2023 14:37:32 GMT
Server: Kestrel
```
* Тело ответа:

отсутствует

**38. GET ​/api​/v1​/Authors​/{1}**

* URL https://fakerestapi.azurewebsites.net/api/v1/Authors/{{id_book}}

* Ожидаемый результат: 200, тест успешно пройден

* Заголовки запроса:
```
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 18d92ada-ddb4-4e73-96b3-1447db6ed714
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
```
* Тело запроса:

отсутствует

* Ожидаемый результат:
```
Content-Type: application/json; charset=utf-8; v=1.0
Date: Sat, 17 Jun 2023 15:12:29 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0
```
* Тело ответа:
```
{"id":1,"idBook":1,"firstName":"First Name 1","lastName":"Last Name 1"}
```

**39. GET ​/api​/v1​/Authors​/{0}**

* URL https://fakerestapi.azurewebsites.net/api/v1/Authors/{{Authors_book_id}}

* Ожидаемый результат: 404  Not Found

* Заголовки запроса:
```
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: b9af36eb-3742-4513-af8e-6cd1c5ab0a58
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
```
* Тело запроса:

отсутствует

* Ожидаемый результат:
```
Content-Type: application/problem+json; charset=utf-8; v=1.0
Date: Sat, 17 Jun 2023 15:11:14 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0
```
* Тело ответа:
```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.4","title":"Not Found","status":404,"traceId":"00-f43e7f131c86b640bc4877017b1809a4-f46a58da9fc9b447-00"}
```

**40. PUT /api​/v1​/Authors​/{id}**

* URL https://fakerestapi.azurewebsites.net/api/v1/Authors/{{id_book}}

* Ожидаемый результат: 200, тест успешно пройден

* Заголовки запроса:
```
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 85d590cb-d920-4bc8-a888-154b6111999d
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
```
* Тело запроса:
```
{
  "id": 0,
  "idBook": 0,
  "firstName": "string",
  "lastName": "string"
}
```
* Ожидаемый результат:
```
Content-Type: application/json; charset=utf-8; v=1.0
Date: Sat, 17 Jun 2023 15:20:57 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0
```
* Тело ответа:
```
{"id":0,"idBook":0,"firstName":"string","lastName":"string"}
```

**41. PUT /api​/v1​/Authors​/{id} - ввод в поле {id}: 123123123123**

* URL https://fakerestapi.azurewebsites.net/api/v1/Authors/{{Authors_for_put_id}}

* Ожидаемый результат: 400 Bad Request

* Заголовки запроса:
```
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 5bf6181f-7057-48f0-beab-f68d4875eb53
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
```
* Тело запроса:
```
{
  "id": 0,
  "idBook": 0,
  "firstName": "string",
  "lastName": "string"
}
```
* Ожидаемый результат:
```
Content-Type: application/problem+json; charset=utf-8
Date: Sat, 17 Jun 2023 15:26:58 GMT
Server: Kestrel
Transfer-Encoding: chunked
```
* Тело ответа:
```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-91ef3e9d657de14380d50fe0e4e9e3b0-18716f2c6eb36042-00","errors":{"id":["The value '123123123123' is not valid."]}}
```

**42. PUT /api​/v1​/Authors​/{id} - ввод в поле {id}: -1(отрицательное число)**

* URL https://fakerestapi.azurewebsites.net/api/v1/Authors/{{Authors_minus_id}}

* Ожидаемый результат: 400 Bad Request (фактический 200)

* Заголовки запроса:
```
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: e721d6fb-f20b-495e-be0c-e1fd735d173d
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
```
* Тело запроса:
```
{
  "id": 0,
  "idBook": 0,
  "firstName": "string",
  "lastName": "string"
}
```
* Ожидаемый результат:
```
Content-Type: application/json; charset=utf-8; v=1.0
Date: Sat, 17 Jun 2023 15:31:37 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0
```
* Тело ответа:
```
{"id":0,"idBook":0,"firstName":"string","lastName":"string"}
```

**43. PUT /api​/v1​/Authors​/{id} - ввод в тело запроса {id}": null**

* URL https://fakerestapi.azurewebsites.net/api/v1/Authors/{{id_book}}

* Ожидаемый результат: 400 Bad Request

* Заголовки запроса:
```
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: fa972e7f-e2bc-448d-808c-3cc44e8ff85d
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
```
* Тело запроса:
```
{
  "id": {{Authors_id}},
  "idBook": 0,
  "firstName": "string",
  "lastName": "string"
}
```
* Ожидаемый результат:
```
Content-Type: application/problem+json; charset=utf-8
Date: Sat, 17 Jun 2023 15:37:23 GMT
Server: Kestrel
Transfer-Encoding: chunked
```
* Тело ответа:
```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-4b2e6e0d2dd02c4f91c6516be817e17d-8b29d47444d59d49-00","errors":{"$.id":["The JSON value could not be converted to System.Int32. Path: $.id | LineNumber: 1 | BytePositionInLine: 9."]}}
```

**44. PUT /api​/v1​/Authors​/{id} - удаление скобки "{" в теле запроса**

* URL https://fakerestapi.azurewebsites.net/api/v1/Authors/{{id_book}}

* Ожидаемый результат: 400 Bad Request

* Заголовки запроса:
```
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: e34c05d8-4550-4eb9-a34f-0fca3b63ff7c
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
```
* Тело запроса:
```
 "id": 0,
  "idBook": 0,
  "firstName": "string",
  "lastName": "string"
}
```
* Ожидаемый результат:
```
Content-Type: application/problem+json; charset=utf-8
Date: Sat, 17 Jun 2023 15:40:50 GMT
Server: Kestrel
Transfer-Encoding: chunked
```
* Тело ответа:
```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-3a4d14a6be6c3a46b89d8939e2d3d9d8-5f29ccc2fd40bd4f-00","errors":{"$":["The JSON value could not be converted to FakeRestAPI.Models.Author. Path: $ | LineNumber: 0 | BytePositionInLine: 6."]}}
```

**45. PUT /api​/v1​/Authors​/{id} - удаление тела запроса**

* URL https://fakerestapi.azurewebsites.net/api/v1/Authors/{{id_book}}

* Ожидаемый результат: 400 Bad Request

* Заголовки запроса:
```
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: e7eef24a-34d9-4986-805a-6b1a4cecd127
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
```
* Тело запроса:

отсутствует

* Ожидаемый результат:
```
Content-Type: application/problem+json; charset=utf-8
Date: Sat, 17 Jun 2023 15:47:18 GMT
Server: Kestrel
Transfer-Encoding: chunked
```
* Тело ответа:
```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-463b4c77b8317444bc7efc9e8efde52d-8f3229e4ea388f4d-00","errors":{"$":["The input does not contain any JSON tokens. Expected the input to start with a valid JSON token, when isFinalBlock is true. Path: $ | LineNumber: 7 | BytePositionInLine: 0."]}}
```

**46. DELETE ​/api​/v1​/Authors​/{id}**

* URL https://fakerestapi.azurewebsites.net/api/v1/Authors/{{id_book}}

* Ожидаемый результат: 200, тест пройден успешно

* Заголовки запроса:
```
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 67e395d6-07b8-4b59-916a-f43a0ab35eed
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
```
* Тело запроса:

отсутствует

* Ожидаемый результат:
```
Content-Length: 0
Date: Sat, 17 Jun 2023 15:50:21 GMT
Server: Kestrel
api-supported-versions: 1.0
```
* Тело ответа:

отсутствует


**47. DELETE ​/api​/v1​/Authors​/{id} - ввод "id":0987654321234567890**

* URL https://fakerestapi.azurewebsites.net/api/v1/Authors/{{Authors_very_bad_id}}

* Ожидаемый результат: 400, Bad Request

* Заголовки запроса:
```
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: d84bbb09-2c12-4af3-870b-d12429058ab1
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
```
* Тело запроса:

отсутствует

* Ожидаемый результат:
```
Content-Type: application/problem+json; charset=utf-8
Date: Sat, 17 Jun 2023 15:54:52 GMT
Server: Kestrel
Transfer-Encoding: chunked
```
* Тело ответа:
```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-038a8acd6baa7a47a13589a7bd2f9b43-e385a50a2873214d-00","errors":{"id":["The value '0987654321234567890' is not valid."]}}
```
## Users

**48. GET /api/v1/Users**

* URL https://fakerestapi.azurewebsites.net/api/v1/Users

* Ожидаемый результат: 200, тест успешно пройден

* Заголовки запроса:
```
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 147d7e87-b8dc-450f-a284-b6a6429f5508
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

```
* Тело запроса:

отсутствует

* Ожидаемый результат:
```
Content-Type: application/json; charset=utf-8; v=1.0
Date: Sat, 17 Jun 2023 20:18:09 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.00
```
* Тело ответа:
```
[{"id":1,"userName":"User 1","password":"Password1"},{"id":2,"userName":"User 2","password":"Password2"},
```

**49. POST /api/v1/Users**

* URL https://fakerestapi.azurewebsites.net/api/v1/Users

* Ожидаемый результат: 200, тест успешно пройден

* Заголовки запроса:
```
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: d87e392e-f9f9-4b08-917f-2c51258676e8
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
```
* Тело запроса:
```
{
  "id": 0,
  "userName": "string",
  "password": "string"
}
```
* Ожидаемый результат:
```
Content-Type: application/json; charset=utf-8; v=1.0
Date: Sat, 17 Jun 2023 20:22:28 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0
```
* Тело ответа:
```
{"id":0,"userName":"string","password":"string"}
```

**50. POST /api/v1/Users: отправка пустых строк в теле запроса**

* URL <https://fakerestapi.azurewebsites.net/api/v1/Users>

* Ожидаемый результат: 400, Bad Request

* Заголовки запроса:
```
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 4307cbe0-bb78-4518-9a54-38b0fa5738ea
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
```
* Тело запроса:
```
{
   
}
```
* Ожидаемый результат:
```
Content-Type: application/json; charset=utf-8; v=1.0
Date: Sat, 17 Jun 2023 20:25:40 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0
```
* Тело ответа:
```
{"id":0,"userName":null,"password":null}
```

**51. POST /api/v1/Users: ввод в теле запроса "id":1234567890987654321**

* URL https://fakerestapi.azurewebsites.net/api/v1/Users

* Ожидаемый результат: 400 Bad Request

* Заголовки запроса:
```
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 36b946ca-86cb-4013-a1d9-41ee21a4872a
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
```
* Тело запроса:
```
{
  "id": {{Users_long_id}},
  "userName": "string",
  "password": "string"
}
```
* Ожидаемый результат:
```
Content-Type: application/problem+json; charset=utf-8
Date: Sat, 17 Jun 2023 20:33:12 GMT
Server: Kestrel
Transfer-Encoding: chunked
```
* Тело ответа:
```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-39e7e271936ef3419327b20f8cb5f2c6-12615f91baabde4d-00","errors":{"$.id":["The JSON value could not be converted to System.Int32. Path: $.id | LineNumber: 1 | BytePositionInLine: 9."]}}
```

**52. POST /api/v1/Users: удаление строки "userName": "string" в теле запроса**

* URL https://fakerestapi.azurewebsites.net/api/v1/Users

* Ожидаемый результат: 400 Bad Request

* Заголовки запроса:
```
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 627f2e0d-37e0-4cf0-91d4-839499fc0a27
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
```
* Тело запроса:
```
{
  "id": 0,
 ,
  "password": "string"
}
```
* Ожидаемый результат:
```
Content-Type: application/problem+json; charset=utf-8
Date: Sat, 17 Jun 2023 20:38:40 GMT
Server: Kestrel
Transfer-Encoding: chunked
```
* Тело ответа:
```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-a0301fc83d35bc4d843d43e5c0437b06-8ea0635886fa1241-00","errors":{"$":["',' is an invalid start of a property name. Expected a '\"'. Path: $ | LineNumber: 2 | BytePositionInLine: 1."]}}
```

**53. GET ​/api​/v1​/Users​/{2}**

* URL https://fakerestapi.azurewebsites.net/api/v1/Users/{{activity_id}}

* Ожидаемый результат: 400 Bad Request

* Заголовки запроса:
```
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 30eaea99-7053-4675-8c52-df7909b35ed0
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
```
* Тело запроса:

отсутствует

* Ожидаемый результат:
```
Content-Type: application/json; charset=utf-8; v=1.0
Date: Sat, 17 Jun 2023 20:42:18 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0
```
* Тело ответа:
```
{"id":2,"userName":"User 2","password":"Password2"}
```

**54. GET ​/api​/v1​/Users​/{0}**

* URL https://fakerestapi.azurewebsites.net/api/v1/Users/{{activ_id}}

* Ожидаемый результат: 404 Not Found

* Заголовки запроса:
```
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: fc1c7acf-fbdd-4784-8c7a-237228194de5
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
```
* Тело запроса:

отсутствует

* Ожидаемый результат:
```
Content-Type: application/problem+json; charset=utf-8; v=1.0
Date: Sat, 17 Jun 2023 20:45:50 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0
```
* Тело ответа:
```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.4","title":"Not Found","status":404,"traceId":"00-1ef5c430d4747d488078909a3966b7de-6b564d725adae847-00"}
```

**55. PUT ​/api​/v1​/Users​/{5}**

* URL https://fakerestapi.azurewebsites.net/api/v1/Users/{{users_id}}

* Ожидаемый результат: 200 тест успешно пройден

* Заголовки запроса:
```
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 626fab52-b439-4d3c-904f-d2a3b434048c
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
```
* Тело запроса:
```
{
  "id": 0,
  "userName": "string",
  "password": "string"
}
```
* Ожидаемый результат:
```
Content-Type: application/json; charset=utf-8; v=1.0
Date: Sat, 17 Jun 2023 20:54:52 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0
```
* Тело ответа:
```
{"id":0,"userName":"string","password":"string"}
```

**56. PUT ​/api​/v1​/Users​/{11111111111111111111}**

* URL https://fakerestapi.azurewebsites.net/api/v1/Users/{{users_long_put_id}}

* Ожидаемый результат: 400 Bad Request

* Заголовки запроса:
```
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 87997198-1bf3-4582-a332-6cb6c0ad13f1
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
```
* Тело запроса:
```
{
  "id": 0,
  "userName": "string",
  "password": "string"
}
```
* Ожидаемый результат:
```
Content-Type: application/problem+json; charset=utf-8
Date: Sat, 17 Jun 2023 21:00:00 GMT
Server: Kestrel
Transfer-Encoding: chunked
```
* Тело ответа:
```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-fb5380338da0aa4ba890a1af9efe4db8-0d66bd6054d01142-00","errors":{"id":["The value '11111111111111111111' is not valid."]}}
```

**57. PUT ​/api​/v1​/Users​/{-7} - ввод отрицательного числа**

* URL https://fakerestapi.azurewebsites.net/api/v1/Users/{{users_minus_id}}

* Ожидаемый результат: 400 Bad Request (фактический 200)

* Заголовки запроса:
```
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 2134e08c-8729-49fe-a1c8-35eb268cebdd
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
```
* Тело запроса:
```
{
  "id": 0,
  "userName": "string",
  "password": "string"
}
```
* Ожидаемый результат:
```
Content-Type: application/json; charset=utf-8; v=1.0
Date: Sat, 17 Jun 2023 21:03:28 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0
```
* Тело ответа:
```
{"id":0,"userName":"string","password":"string"}
```

**58. PUT ​/api​/v1​/Users​/{1} - изменение строки "userName": "string" на null**

* URL https://fakerestapi.azurewebsites.net/api/v1/Users/{{id_book}}

* Ожидаемый результат: 400 Bad Request 

* Заголовки запроса:
```
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 1168af39-77a5-4a74-a031-1bc6bcc54db6
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
```
* Тело запроса:
```
{
  "id": 0,
  null,
  "password": "string"
}
```
* Ожидаемый результат:
```
Content-Type: application/problem+json; charset=utf-8
Date: Sat, 17 Jun 2023 21:07:41 GMT
Server: Kestrel
Transfer-Encoding: chunked
```
* Тело ответа:
```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-0efebe7e0304554493e4fdd1e3a57fae-53be403b5b01a84e-00","errors":{"$":["'n' is an invalid start of a property name. Expected a '\"'. Path: $ | LineNumber: 2 | BytePositionInLine: 2."]}}
```

**59. PUT ​/api​/v1​/Users​/{2} - удаление  скобок {} в теле запроса**

* URL https://fakerestapi.azurewebsites.net/api/v1/Users/{{activity_id}}

* Ожидаемый результат: 400 Bad Request 

* Заголовки запроса:
```
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 6fb96323-face-4158-99c9-3e8b962ef91d
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
```
* Тело запроса:
```
"id": 0,
  "userName": "string",
  "password": "string"
```
* Ожидаемый результат:
```
Content-Type: application/problem+json; charset=utf-8
Date: Sat, 17 Jun 2023 21:11:46 GMT
Server: Kestrel
Transfer-Encoding: chunked
```
* Тело ответа:
```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-ae511de907c9e949aa4245ce05132b47-8d576474bc15bd4a-00","errors":{"$":["The JSON value could not be converted to FakeRestAPI.Models.User. Path: $ | LineNumber: 0 | BytePositionInLine: 4."]}}
```

**60. PUT ​/api​/v1​/Users​/{3} - удаление тела запроса**

* URL https://fakerestapi.azurewebsites.net/api/v1/Users/{{activ_for_put}}

* Ожидаемый результат: 400 Bad Request (фактический 200)

* Заголовки запроса:
```
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: e4b2af7a-3613-4621-9d7f-6b83e68d35cc
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
```
* Тело запроса:
```

```
* Ожидаемый результат:
```
Content-Type: application/problem+json; charset=utf-8
Date: Sat, 17 Jun 2023 21:19:13 GMT
Server: Kestrel
Transfer-Encoding: chunked
```
* Тело ответа:
```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-d598cbe65494ad4a933008dcf5667c07-6870f51d09eb4046-00","errors":{"$":["The input does not contain any JSON tokens. Expected the input to start with a valid JSON token, when isFinalBlock is true. Path: $ | LineNumber: 3 | BytePositionInLine: 0."]}}
```

**61. DELETE ​/api​/v1​/Users​/{8}**

* URL https://fakerestapi.azurewebsites.net/api/v1/Users/{{Users_del_id}}

* Ожидаемый результат: 200 

* Заголовки запроса:
```
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 1786f6b5-33af-414a-a579-7d2c0727f2e0
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
```
* Тело запроса:

отсутствует

* Ожидаемый результат:
```
Content-Length: 0
Date: Sat, 17 Jun 2023 21:22:40 GMT
Server: Kestrel
api-supported-versions: 1.0
```
* Тело ответа:
```

```
**62. DELETE ​/api​/v1​/Users​/{9876987698769876}**

* URL https://fakerestapi.azurewebsites.net/api/v1/Users/{{Users_del_long_id}}

* Ожидаемый результат: 400 Bad Request 

* Заголовки запроса:
```
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 86e75729-25df-462c-a245-f369a55f0ff6
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
```
* Тело запроса:

отсутствует

* Ожидаемый результат:
```
Content-Type: application/problem+json; charset=utf-8
Date: Sat, 17 Jun 2023 21:26:45 GMT
Server: Kestrel
Transfer-Encoding: chunked
```
* Тело ответа:
```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-24189baaa7e5d44db17d0b1db5bbf2d3-807a91f601cd614f-00","errors":{"id":["The value '9876987698769876' is not valid."]}}
```

## Books

**63. GET /api/v1/Books**

* URL <https://fakerestapi.azurewebsites.net/api/v1/Books>

* Ожидаемый результат: 200, тест успешно пройден

* Заголовки запроса:

```
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: cce407ce-a9f4-406d-b4e1-320a234ed0e8
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
```

* Тело запроса:

отсутствует

* Ожидаемый результат:

```
Content-Type: application/json; charset=utf-8; v=1.0
Date: Sun, 18 Jun 2023 12:19:10 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0
```

* Тело ответа:

```
[{"id":1,"title":"Book 1","description":"Diam quis consetetur sit sanctus rebum ... nulla duis amet consectetuer.\n","pageCountclita possim blandit et aliquyam erat facilisis labore possim.\n","publishDate":"2022-11-30T12:19:10.4698958+00:00"}]
```

**64. POST /api/v1/Books**

* URL <https://fakerestapi.azurewebsites.net/api/v1/Books>

* Ожидаемый результат: 400 Error: Bad Request

* Заголовки запроса:

```
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 65da1cbb-7a12-4d1a-8994-6bbc44984427
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
```

* Тело запроса:

```
{
  "id": 0,
  "title": "string",
  "description": "string",
  "pageCount": 0,
  "excerpt": "string",
  "publishDate": "2023-06-18T11:33:40.771Z"
}
```

* Ожидаемый результат:

```
Content-Type: application/json; charset=utf-8; v=1.0
Date: Sun, 18 Jun 2023 12:19:10 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0
```

* Тело ответа:

```
{"id":0,"title":"string","description":"string","pageCount":0,"excerpt":"string","publishDate":"2023-06-18T11:33:40.771Z"}
```

**65. POST​ /api​/v1​/Books​: ввод в тело запроса "id": 8585858585**

* URL <https://fakerestapi.azurewebsites.net/api/v1/Books>

* Ожидаемый результат: 404 Error: Not Found

* Заголовки запроса:

```
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: fc3d973c-ffb1-415e-997f-abf48a5e4af6
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
```

* Тело запроса:

```
{
  "id": {{Book_long_id}},
  "title": "string",
  "description": "string",
  "pageCount": 0,
  "excerpt": "string",
  "publishDate": "2023-06-18T11:33:40.771Z"
}
```

* Ожидаемый результат:

```
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 18 Jun 2023 14:14:38 GMT
Server: Kestrel
Transfer-Encoding: chunked
```

* Тело ответа:

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-c368b0e60a5f4e44930558b3f504d112-0c0430b3216c0b42-00","errors":{"$.id":["The JSON value could not be converted to System.Int32. Path: $.id | LineNumber: 1 | BytePositionInLine: 9."]}}
```

**66. POST /api/v1/Books: ввод в тело запроса "id": null**

* URL <https://fakerestapi.azurewebsites.net/api/v1/Books>

* Ожидаемый результат: 400 Error: Bad Request

* Заголовки запроса:

```
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 22b14aa4-bab1-4811-bb9a-316c50322018
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
```

* Тело запроса:

```
{
  "id": null,
  "title": "string",
  "description": "string",
  "pageCount": 0,
  "excerpt": "string",
  "publishDate": "2023-06-18T11:33:40.771Z"
}
```

* Ожидаемый результат:

```
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 18 Jun 2023 14:31:02 GMT
Server: Kestrel
Transfer-Encoding: chunked
```

* Тело ответа:

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-ed1f22d2af674a4080907dc892beb215-36528d248b0f2046-00","errors":{"$.id":["The JSON value could not be converted to System.Int32. Path: $.id | LineNumber: 1 | BytePositionInLine: 12."]}}
```

**67. POST /api​/v1​/Books​: ввод в тело  запроса "id": 2**

* URL <https://fakerestapi.azurewebsites.net/api/v1/Books>

* Ожидаемый результат: 200, тест успешно пройден

* Заголовки запроса:

```
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 8f0c78a8-ef6b-43a1-9743-76b86fb5554e
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
```

* Тело запроса:

```
{
  "id": 2,
  "userName": "string",
  "password": "string"
}
```

* Ожидаемый результат:

```
Content-Type: application/json; charset=utf-8; v=1.0
Date: Sun, 18 Jun 2023 15:00:28 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0
```

* Тело ответа:

```
{"id":2,"title":null,"description":null,"pageCount":0,"excerpt":null,"publishDate":"0001-01-01T00:00:00"}
```

**68. GET ​/api​/v1​/Books​/{0}**

* URL <https://fakerestapi.azurewebsites.net/api/v1/Books/0>

* Ожидаемый результат: 404 Error: Not Found

* Заголовки запроса:

```
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: b77d9189-73f8-4a0a-ae14-987ee09170dd
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
```

* Тело запроса:

отсутствует

* Ожидаемый результат:

```
Content-Type: application/problem+json; charset=utf-8; v=1.0
Date: Sun, 18 Jun 2023 15:23:48 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0
```

* Тело ответа:

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.4","title":"Not Found","status":404,"traceId":"00-04919b48c7c0cc429b92fa54121a352d-14a69cbad3240640-00"}
```

**69. GET​ /api​/v1​/Books​/{8}**

* URL <https://fakerestapi.azurewebsites.net/api/v1/Books/8>

* Ожидаемый результат: 200, тест успешно пройден

* Заголовки запроса:

```
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 18d6ae5e-0c50-47ef-a3a7-3196e8da6661
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
```

* Тело запроса:

отсутствует

* Ожидаемый результат:

```
Content-Type: application/json; charset=utf-8; v=1.0
Date: Sun, 18 Jun 2023 15:40:59 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0
```

* Тело ответа:

```
{"id":8,"title":"Book 8","description":"Elit erat dolor magna vel dolor ut sadipscing sed takimata eu diam ipsum no lorem diam amet volutpat sanctus. No aliquam diam accusam. Odio lorem id vero diam et elitr labore vulputate ipsum velit sed at. Nulla dolores in lorem lorem invidunt no aliquyam diam diam. Rebum stet et ut consetetur. Et in illum kasd diam laoreet justo amet ut diam justo facilisi takimata consequat eos facilisi gubergren tempor id. Invidunt eirmod lobortis dolores in autem luptatum vero diam dolore iusto consequat nulla. Eros eirmod elitr sit duo ut qui stet facilisis amet clita sed et gubergren. Tincidunt sed sit at esse at tempor est commodo assum. Gubergren eum et dolores justo dignissim diam autem at gubergren et tincidunt sit duo et.\n","pageCount":800,"excerpt":"Nulla sanctus labore wisi diam tempor et at et sea sea dolores nonumy dolores feugait clita nibh. Nonumy ipsum magna vero at rebum duo molestie diam no. Duis sed duo diam nostrud nonummy iriure diam dolor ipsum voluptua. Elitr sit aliquyam justo amet invidunt tempor takimata aliquyam consetetur clita aliquyam amet clita dolor autem et dolor est. Aliquyam et takimata dolores diam sed sit accusam stet quod vel facilisis vel ipsum nonumy nulla.\nJusto commodo amet ut sit ut et tempor voluptua ea lorem dolor erat feugiat. Ipsum et consequat nulla rebum erat no accumsan eos consetetur voluptua duis placerat dolor dolor suscipit dolores ex. Erat et sed sed eirmod et dolor dolor magna aliquyam sanctus.\nAmet minim duis sed ullamcorper takimata luptatum et gubergren vulputate diam placerat et sed dolor aliquip vero elitr eu. At nisl sanctus et elitr at dolore et sed nostrud stet amet diam magna. Voluptua amet vero dolor gubergren ipsum kasd justo ipsum sit est in sanctus dolore.\nLorem et iusto sit minim at dolor invidunt in aliquip praesent sanctus ea amet diam quis invidunt. Dolore dolores labore sed et sea sed. Nonumy dignissim odio vero et labore sadipscing suscipit at clita invidunt aliquyam. Sit takimata velit elitr feugiat est suscipit odio. Eos et est elitr aliquyam lorem takimata rebum dolores ea dolore magna aliquip at ipsum suscipit enim. Clita gubergren invidunt et vero nonumy labore magna gubergren autem ad clita lorem sed clita rebum. Aliquyam dolor ut lorem nostrud congue clita. At consetetur esse rebum et no tempor kasd tempor hendrerit exerci lorem sea. Magna no feugait accusam dolore ea vero kasd at dolor facer sanctus et nisl diam aliquyam. Aliquyam at nisl amet sit luptatum sanctus diam invidunt iriure nobis et tempor ut gubergren eirmod. Dignissim blandit feugiat takimata. Veniam lorem ea justo et vero vulputate elitr dolor et takimata sea kasd erat stet ut invidunt quis. Erat amet volutpat et consetetur assum et magna vero nonumy. Sed dignissim kasd sanctus feugiat praesent. Sed molestie et suscipit amet kasd vel no sit ipsum rebum ipsum sadipscing aliquam no diam.\nConsetetur et et amet lorem kasd. Volutpat ex dolor suscipit nihil est et cum illum no voluptua et nulla amet sadipscing at. Sadipscing aliquyam dolores tempor autem ipsum eirmod nonummy praesent lobortis molestie ut ea kasd molestie. Diam elitr nulla duo. Praesent lorem consetetur dolore no feugait et blandit justo nibh volutpat takimata dignissim. Et consetetur rebum at diam. At amet erat ut et eirmod lorem te sit nonumy takimata illum in amet at magna eos est. Aliquam amet diam kasd diam stet at iriure lorem quis sed voluptua et dolore. Nihil molestie lorem no lorem est tation in velit magna wisi invidunt dolores dolore dolore. Et takimata et praesent lorem ipsum nihil placerat eos sanctus hendrerit. Dolor odio stet suscipit eos ullamcorper dolores. Ea stet takimata adipiscing stet gubergren nostrud invidunt tempor vero imperdiet sanctus eos diam sea sit amet qui lorem. Vero takimata invidunt elitr dolor aliquyam quod. Amet gubergren molestie euismod at sadipscing suscipit. Ex no ipsum nonumy sea sit ipsum erat at lorem lobortis takimata. Tation ipsum augue dolor commodo rebum sea diam rebum ut. Ut in sit et diam dolores autem aliquyam ea illum eirmod ut erat ea voluptua clita at.\n","publishDate":"2023-06-10T15:40:59.4460953+00:00"}
```

**70. GET ​/api​/v1​/Books​/{12344321}**

* URL <https://fakerestapi.azurewebsites.net/api/v1/Books/12344321>

* Ожидаемый результат: 404 Error: Not Found

* Заголовки запроса:

```
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 712b681c-cd94-4afd-a168-ed8476930581
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
```

* Тело запроса:

отсутствует

* Ожидаемый результат:

```
Content-Type: application/problem+json; charset=utf-8; v=1.0
Date: Sun, 18 Jun 2023 16:45:13 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0
```

* Тело ответа:

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.4","title":"Not Found","status":404,"traceId":"00-c46accbe8816274199780375321ac96b-fd710a58e7d65045-00"}
```

**71. PUT /api/v1/Books/{id}: ввод в тело запроса "id" null**

* URL <https://fakerestapi.azurewebsites.net/api/v1/Books/{{id_book}}>

* Ожидаемый результат: 400 Bad Request

* Заголовки запроса:

```
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 8e5b4f6d-b3d7-4115-bff0-71f7ab1aec3c
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
```

* Тело запроса:

```
{
  "id": null,
  "title": "string",
  "description": "string",
  "pageCount": 0,
  "excerpt": "string",
  "publishDate": "2023-06-18T11:44:30.814Z"
}
```

* Ожидаемый результат:

```
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 18 Jun 2023 16:08:44 GMT
Server: Kestrel
Transfer-Encoding: chunked
```

* Тело ответа:

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-9680e69e25721e47bea9d2e9e8c3db95-38a996a7fd5f6141-00","errors":{"$.id":["The JSON value could not be converted to System.Int32. Path: $.id | LineNumber: 1 | BytePositionInLine: 12."]}}
```

**72. PUT /api​/v1​/Books/{12344321}**

* URL <https://fakerestapi.azurewebsites.net/api/v1/Books/12344321>

* Ожидаемый результат: 200, тест успешно пройден

* Заголовки запроса:

```
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 9c200313-696e-4210-96c0-5261d2b4a648
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
```

* Тело запроса:

```
{
  "id": 0,
  "userName": "string",
  "password": "string"
}
```

* Ожидаемый результат:

```
Content-Type: application/json; charset=utf-8; v=1.0
Date: Sun, 18 Jun 2023 16:50:52 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0
```

* Тело ответа:

```
{"id":0,"title":null,"description":null,"pageCount":0,"excerpt":null,"publishDate":"0001-01-01T00:00:00"}
```

**73. PUT /api/v1/Books/{id}: ввод в тело запроса "id" null**

* URL <https://fakerestapi.azurewebsites.net/api/v1/Books/1>

* Ожидаемый результат: 400 Bad Request

* Заголовки запроса:

```
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 830bdbc0-2aef-46f8-bd03-02ed5ab7bfeb
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
```

* Тело запроса:

```
{
  "id": null,
  "title": "string",
  "description": "string",
  "pageCount": 0,
  "excerpt": "string",
  "publishDate": "2023-06-18T11:44:30.814Z"
}
```

* Ожидаемый результат:

```
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 18 Jun 2023 16:53:48 GMT
Server: Kestrel
Transfer-Encoding: chunked
```

* Тело ответа:

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-a700f61f40d3ba45b801a293a83ae658-cd888b064cc76745-00","errors":{"$.id":["The JSON value could not be converted to System.Int32. Path: $.id | LineNumber: 1 | BytePositionInLine: 12."]}}
```

**74. POST /api/v1/Books**

* URL <https://fakerestapi.azurewebsites.net/api/v1/Books>

* Ожидаемый результат: 200, тест успешно пройден

* Заголовки запроса:

```
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 3c27038b-7bcb-47b8-be9e-13cdb6dd5066
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
```

* Тело запроса:

```
{
  "id": 0,
  "userName": "string",
  "password": "string"
}
```

* Ожидаемый результат:

```
Content-Type: application/json; charset=utf-8; v=1.0
Date: Sun, 18 Jun 2023 17:01:02 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0
```

* Тело ответа:

```
{"id":0,"title":null,"description":null,"pageCount":0,"excerpt":null,"publishDate":"0001-01-01T00:00:00"}
```

**75. PUT /api/v1/Books: ввод в тело  запроса "id" 12344321**

* URL <https://fakerestapi.azurewebsites.net/api/v1/Users/0>

* Ожидаемый результат: 200, тест успешно пройден

* Заголовки запроса:

```
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 23b7bce9-b301-47dc-93ec-99828d30d75e
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
```

* Тело запроса:

```
{
  "id": 12344321,
  "userName": "string",
  "password": "string"
}
```

* Ожидаемый результат:

```
Content-Type: application/json; charset=utf-8; v=1.0
Date: Sun, 18 Jun 2023 17:18:41 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0
```

* Тело ответа:

```
{"id":12344321,"title":null,"description":null,"pageCount":0,"excerpt":null,"publishDate":"0001-01-01T00:00:00"}
```

**76. PUT /api/v1/Books: удаление скобки } в теле запроса**

* URL <https://fakerestapi.azurewebsites.net/api/v1/Users/0>

* Ожидаемый результат: 400 Error: Bad Request

* Заголовки запроса:

```
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 41c02031-7d52-48fc-b740-2a547e2dbc11
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
```

* Тело запроса:

```
{
  "id": 0,
  "userName": "string",
  "password": "string"
```

* Ожидаемый результат:

```
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 18 Jun 2023 17:27:10 GMT
Server: Kestrel
Transfer-Encoding: chunked
```

* Тело ответа:

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-c2a457495aab93408114b5725c876cbd-90ef46215bd0004b-00","errors":{"$":["Expected depth to be zero at the end of the JSON payload. There is an open JSON object or array that should be closed. Path: $ | LineNumber: 4 | BytePositionInLine: 0."]}}
```

**77. PUT /api/v1/Books: удаление строки "title"**

* URL <https://fakerestapi.azurewebsites.net/api/v1/Books>

* Ожидаемый результат: 200, тест успешно пройден

* Заголовки запроса:

```
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 431496e6-964c-429b-81c3-3f922e7effaf
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
```

* Тело запроса:

```
{
  "userName": "string",
  "password": "string"
}
```

* Ожидаемый результат:

```
Content-Type: application/json; charset=utf-8; v=1.0
Date: Sun, 18 Jun 2023 17:33:55 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0
```

* Тело ответа:

```
{"id":0,"title":null,"description":null,"pageCount":0,"excerpt":null,"publishDate":"0001-01-01T00:00:00"}
```

**78. DELETE /api​/v1​/Books/{5}**

* URL <https://fakerestapi.azurewebsites.net/api/v1/Books>

* Ожидаемый результат: 200, тест успешно пройден

* Заголовки запроса:

```
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: fc50a95c-ff33-4c48-b517-7027fbd92455
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
```

* Тело запроса:

отсутствует

* Ожидаемый результат:

```
Content-Length: 0
Date: Sun, 18 Jun 2023 17:38:50 GMT
Server: Kestrel
api-supported-versions: 1.0
```

* Тело ответа:

отсутствует

**79. DELETE /api​/v1​/Books​/{12345678900987654321}**

* URL <https://fakerestapi.azurewebsites.net/api/v1/Books>

* Ожидаемый результат: 400 Bad Request

* Заголовки запроса:

```
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: ca29be35-3939-4b27-bb55-82837e88aa6c
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
```

* Тело запроса:

отсутствует

* Ожидаемый результат:

```
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 18 Jun 2023 17:45:03 GMT
Server: Kestrel
Transfer-Encoding: chunked
```

* Тело ответа:

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-c4597ac88b5bce419d92babf8082dbb7-3b5de91439bc754d-00","errors":{"id":["The value '12345678900987654321' is not valid."]}}
```
